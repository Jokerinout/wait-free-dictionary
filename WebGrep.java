package fr.univnantes.multicore.tp3;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class WebGrep {

	private static Dictionary explored = new Dictionary();

	private static ExecutorService executor;
	
	public static LinkedBlockingQueue<ParsedPage> queue = new LinkedBlockingQueue<ParsedPage>(); 
	
	// nombre de threads qui affichent
	public static final int NUMTHREADSPRINTS = 2;
	
	public WebGrep(int n) {
		executor = Executors.newFixedThreadPool(n);
	}
	
	public static void explore(String address) {
		 if(explored.add(address)) {
			// Parse the page to find matches and hypertext links
			Runnable worker = new WorkerThread(address);
			executor.execute(worker);
		}
	}
	
	public static void print() throws InterruptedException {
		Runnable taskPrint = new PrintTask();
		Thread t = new Thread(taskPrint);
		t.start();
	}

	public static void main(String[] args) throws InterruptedException, IOException {
		// Initialize the program using the options given in argument
		if(args.length == 0) Tools.initialize("-celt --threads=8 Nantes https://fr.wikipedia.org/wiki/Nantes");
		else Tools.initialize(args);
		
		WebGrep web = new WebGrep(Tools.numberThreads());
		
		// Get the starting URL given in argument
		for(String address : Tools.startingURL()) 
			explore(address);
		for(int i=0; i< NUMTHREADSPRINTS;i++) {
			print();
		}
		
	}
}
