package fr.univnantes.multicore.tp3;

class PrintTask implements Runnable {
    
    public void run() {
    	while(true) {
    		try {
				Tools.print(WebGrep.queue.take());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
    	}
    }
}  