package fr.univnantes.multicore.tp3;

import java.io.IOException;

class WorkerThread implements Runnable {  
    private String address;
    
    public WorkerThread(String s){  
        this.address=s;  
    }  
    
    public void run() {  
    	 try {
			ParsedPage page = Tools.parsePage(address);
			if(!page.matches().isEmpty()) {
				WebGrep.queue.put(page);
				for(String href : page.hrefs()) WebGrep.explore(href);
			}
		 } catch (InterruptedException | IOException e) {
			e.printStackTrace();
		}
    }
}  